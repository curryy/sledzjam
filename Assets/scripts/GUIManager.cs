﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GUIManager : MonoBehaviour {
	Text points;
	Text megaPoints;
	GameObject upgradeScroll;
	GameObject canvas;
	bool isMenu;
	public GameObject townHall;
	int menu;

	void Awake(){
		points = GameObject.Find("Points").GetComponent<Text>();
		megaPoints = GameObject.Find("MegaPoints").GetComponent<Text>();
		upgradeScroll = GameObject.Find ("Scroll View") as GameObject;
		canvas = GameObject.Find ("Canvas") as GameObject;
		isMenu = true;
		townHall = GameObject.Find ("townhall");
		townHall.SetActive (false);
	}

	void Start(){
		
	}

	void Update(){
		if(Input.GetKeyDown(KeyCode.C)){
			isMenu = !isMenu;
			canvas.SetActive (isMenu);
		}
	}

	public void OpenClicker(){
		GameObject go = GameObject.Find ("Shop") as GameObject;
		go.SetActive (true);
	}

	public void OpenStatistics(){
		GameObject go = GameObject.Find ("Shop") as GameObject;
		go.SetActive (false);
	}
		
	public void UpdatePoints(){
		megaPoints.text = "" + ClickerManager.instance.points;
		points.text = "" + ClickerManager.instance.points;
		//AddUpgrade ();
	}

	public void ShowTownHall(){
		townHall.SetActive (true);
	}

	
}
