﻿using UnityEngine;
using System.Collections;

public class BubbleSpawner : MonoBehaviour {
	public Object bubblePerfarb;
	public TimerUtil spawnTime;
	public bool isSpawning = false;

	// Use this for initialization
	void Start () {
		spawnTime = new TimerUtil(SledzUpgrades.spawnLimit);
	}

	// Update is called once per frame
	void Update () {
		if (isSpawning) {
			if (spawnTime.IsLimitReached()) {
				for (int i = 0; i < 2f; i++) {
					RandomSpawn();
				}

			}
			spawnTime.Update();
		}
	}

	private void RandomSpawn() {
		Instantiate(bubblePerfarb);
	}
}
