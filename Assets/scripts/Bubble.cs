﻿using UnityEngine;
using System.Collections;

public class Bubble : MonoBehaviour {

	float speed;
	// Use this for initialization
	void Start () {
		speed = Random.Range(Statics.MINSPEED, Statics.MAXSPEED - 0.7f);
		RandomPosition ();
		RandomScale ();
	}
	
	// Update is called once per frame
	void Update () {
		transform.position += transform.up * Time.deltaTime * speed;
		if(transform.position.y >= Statics.YBOUNDARIES * 3f) {
			Destroy(gameObject);
		}
	}

	private void RandomPosition() {
		float randomx = Random.Range(
			0f, +Statics.XBOUNDARIES - 0.2f);
		transform.position = new Vector3(randomx, 0f);
	}


	private void RandomScale(){
		float randomSize = Random.Range(0.05f, 0.7f);
		transform.localScale = transform.localScale * randomSize;
	}
}
