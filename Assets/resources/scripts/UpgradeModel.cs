﻿public class UpgradeModel {

	public string name { get; set; }
    public int amount { get; set; }
    public float price { get; set; }
    public bool disposable { get; set; }
    public bool isActive { get; set; }
    public float startPrice { get; set; }

    public UpgradeModel (string name, float price, bool disposable)
    {
        this.name = name;
        this.price = price;
        this.disposable = disposable;
        isActive = false;
        startPrice = price;
    }
}
