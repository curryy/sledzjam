﻿using UnityEngine;
using System.Collections;

public class EnemySpawner : MonoBehaviour {
    public Object prefabNormal;
    public TimerUtil spawnTime;
    public bool isSpawning = false;
    
	// Use this for initialization
	void Start () {
        spawnTime = new TimerUtil(SledzUpgrades.spawnLimit);
	}
	
	// Update is called once per frame
	void Update () {
        if (isSpawning) {
            if (spawnTime.IsLimitReached()) {
                for (int i = 0; i < SledzUpgrades.spawnAtTime; i++) {
                    RandomSpawn();
                }

            }
            spawnTime.Update();
        }
        
       
	}

    private void RandomSpawn() {
        float random = Random.Range(0, 100);
        if (random <= 70f) {
            GameObject normal = Instantiate(prefabNormal) as GameObject;
            normal.GetComponent<SpriteRenderer>().color = new Color(247f / 255f, 0 / 255f, 255 / 255f);
        }else if(random > 70 && random <=80) {
            GameObject black = Instantiate(prefabNormal) as GameObject;
            black.GetComponent<SpriteRenderer>().color = new Color(0.3f, 0.3f, 0.3f);
        }
        else if (random > 80) {
            GameObject toxic = Instantiate(prefabNormal) as GameObject;
            toxic.GetComponent<SpriteRenderer>().color = Color.blue;
            toxic.tag = "Toxic";
        }
        

    }
}
