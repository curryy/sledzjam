﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

    float speed;
    Vector3 startPosition;
	// Use this for initialization
	void Start () {
        speed = Random.Range(Statics.MINSPEED, Statics.MAXSPEED);

        RandomPosition();
        RandomSize();
        RandomRotation();
    }

    private void RandomRotation() {
        float randomRoataion = Random.Range(Statics.MINANGLE, Statics.MAXANGLE);
        transform.Rotate(0, 0, randomRoataion);
    }

    private void RandomSize() {
        float randomSize = Random.Range(Statics.MINSIZE, Statics.MAXSIZE);
        transform.localScale = transform.localScale * randomSize;
    }

    private void RandomPosition() {
        float randomY = Random.Range(
                    -Statics.YBOUNDARIES + 0.2f, +Statics.YBOUNDARIES - 0.2f);
        transform.position = new Vector3(11f, randomY);
    }

    // Update is called once per frame
    void Update () {
        transform.position += -transform.right * Time.deltaTime * speed;
        if(transform.position.y > Statics.YBOUNDARIES || transform.position.y < -Statics.YBOUNDARIES) {
            Vector3 rotation = transform.rotation.eulerAngles;
            rotation.z = rotation.z * -1f;
            transform.rotation = Quaternion.Euler(rotation);
            transform.position += Vector3.left * Time.deltaTime * speed;
        }
        if(transform.position.x <= -Statics.XBOUNDARIES * 1.5f) {
            Destroy(gameObject);
        }
    }

}
