﻿using UnityEngine;
using System.Collections;

public class Statics  {
    //moving space
    public static readonly float YBOUNDARIES = 4.2f;
    public static readonly float XBOUNDARIES = 7.6f;

    //enenmy values
    public static readonly float MAXSPEED = 10f;
    public static readonly float MINSPEED = 1f;
    public static readonly float MAXSIZE = 0.8f;
    public static readonly float MINSIZE = 0.3f;
    public static readonly float MINANGLE = -30;
    public static readonly float MAXANGLE = 30f;


}
