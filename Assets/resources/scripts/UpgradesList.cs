﻿using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.ImageEffects;

public class UpgradesList {
    public static List<UpgradeModel> list;


    public UpgradesList()
    {
        list = new List<UpgradeModel>();
        list.Add(new UpgradeModel("good stuff", 5, false));
        list.Add(new UpgradeModel("lols ima faster", 10, false));
        list.Add(new UpgradeModel("Forward move", 15, true));
        list.Add(new UpgradeModel("Backward move", 200, true));
        
        list.Add(new UpgradeModel("OMG! WTF! Lasers!", 200, false));
        list.Add(new UpgradeModel("Fishes more often", 50, false));
        list.Add(new UpgradeModel("More fishes!!", 50, false));
        list.Add(new UpgradeModel("Multiply points", 10, false));

        list.Add(new UpgradeModel("omg EXPLOSIONS!", 400, true));
        list.Add(new UpgradeModel("It's time for happy Townhall", 2000, true));
        list.Add(new UpgradeModel("GO BIGGER!", 500, false));
		list.Add(new UpgradeModel("disco cure", 1000, false));

        list.Add(new UpgradeModel("Detox! Detox! Detox!", 300, false));
        list.Add(new UpgradeModel("Resurect", 30, false));
  		list.Add(new UpgradeModel("town hall", 1000, true));
	}
    
    public static void deleteItem (UpgradeModel upgrade)
    {
        foreach (UpgradeModel item in list)
        {
            if (upgrade == item)
            {
                switch (item.name)
                {
                    case "lols ima faster":
                        SledzUpgrades.speed += 0.3f;
                        break;
                    case "Forward move":
                        SledzUpgrades.forwardMove = true;
                        break;
                    case "Backward move":
                        SledzUpgrades.backwardMove = true;
                        break;
                    case "Resurect":
                        Camera.allCameras[0].GetComponent<SledzRevier>().Review();
                        break;
                    case "OMG! WTF! Lasers!":
                        SledzUpgrades.isLaserActive = true;
                        SledzUpgrades.laserTimer = 0.9f * SledzUpgrades.laserTimer;
                        SledzUpgrades.ChangeLaserTimeLimit(SledzUpgrades.laserTimer);
                        break;
                    case "Fishes more often":
                        SledzUpgrades.spawnLimit = 0.9f * SledzUpgrades.spawnLimit;
                        SledzUpgrades.ChangeSpawnTimeLimit(SledzUpgrades.spawnLimit);
                        break;
                    case "More fishes!!":
                        SledzUpgrades.spawnAtTime += 1;
                        break;
                    case "Multiply points":
                        SledzUpgrades.pointMultipler += 1;
                        break;
                    case "Detox! Detox! Detox!":
                        SledzUpgrades.areWeOnDrugs = false;
						GameObject.FindGameObjectWithTag("Hero").GetComponent<SledzMoving>().TurnOffDrugMode();
                        break;
                    case "omg EXPLOSIONS!":
                        SledzUpgrades.areExplosionsAviable = true;
                        break;
                    case "It's time for happy Townhall":
						GameObject.Find ("GUIManager").SendMessage ("ShowTownHall");
                        break;
                    case "GO BIGGER!":
                        SledzSizer.sizer += 0.1f;
                        break;
                    case "good stuff":
                        GameObject.FindGameObjectWithTag("DISCO").GetComponent<AudioSource>().Play();
                        GameObject.FindGameObjectWithTag("NormalMusic").GetComponent<AudioSource>().Pause();
                        break;
                    case "disco cure":
                        GameObject.FindGameObjectWithTag("NormalMusic").GetComponent<AudioSource>().Play();
                        GameObject.FindGameObjectWithTag("DISCO").GetComponent<AudioSource>().Pause();
                        break;
                }
                item.amount++;
                    item.price = item.startPrice * (1 + item.amount);

            }
        }
    }

    public UpgradeModel getItemAt(int index)
    {
        return list[index];
    }
}
