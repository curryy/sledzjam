﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ButtonScript : MonoBehaviour
{

    public UpgradeModel upgrade;
    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
        if (!upgrade.disposable)
        {
            if (ClickerManager.instance.points >= upgrade.price)
                upgrade.isActive = true;
            else
                upgrade.isActive = false;
        }
        else
        {
            if (ClickerManager.instance.points >= upgrade.price && upgrade.amount == 0)
                upgrade.isActive = true;
            else
                upgrade.isActive = false;
        }
        if (upgrade.name == "Resurect")
        {
            if (GameObject.FindGameObjectWithTag("Hero") == null && ClickerManager.instance.points >= upgrade.price)
            {
                upgrade.isActive = true;
            }
            else
            {
                upgrade.isActive = false;
            }
                
        }

        if (upgrade.name == "Detox! Detox! Detox!")
        {
            if (!SledzUpgrades.areWeOnDrugs)
                upgrade.isActive = false;
        }
        gameObject.GetComponent<Button>().interactable = upgrade.isActive;
    }

    public void setText(string text)
    {
        gameObject.GetComponentInChildren<Text>().text = text;
    }

    public UpgradeModel getUpgradeModel()
    {
        return this.upgrade;
    }
    public void setUpgrade(UpgradeModel up)
    {
        upgrade = up;
    }

    public void setDiffrentColor()
    {
        gameObject.GetComponent<Image>().color = new Color(255, 0, 0);

    }

    public void buyItem()
    {
        ClickerManager.instance.AddPoints(-(upgrade.price));
        UpgradesList.deleteItem(upgrade);
        setText(upgrade.name + "  - " + upgrade.price);
    }
}
