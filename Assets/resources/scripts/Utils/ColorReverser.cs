﻿using UnityEngine;
using System.Collections;

public class ColorReverser : MonoBehaviour {
    TimerUtil colorTimer;
    // Use this for initialization
    void Start() {
        colorTimer = new TimerUtil(0.25f);
    }

    // Update is called once per frame
    void Update() {
    }

    public void InvertAllMaterialColors() {
        Renderer[] renderers = FindObjectsOfType(typeof(Renderer)) as Renderer[];
        foreach (Renderer rend in renderers) {
            if (rend.material.HasProperty("_Color")) {
                rend.material.color = InvertColor(rend.material.color);
            }
        }
    }

    private Color InvertColor(Color color) {
        return new Color(1.0f - color.r, 1.0f - color.g, 1.0f - color.b);
    }
}
