﻿using UnityEngine;
using System.Collections;

public class SledzSizer : MonoBehaviour {
    public static float sizer = 0.5f;
	// Use this for initialization
	void Start () {

    }
	
	// Update is called once per frame
	void Update () {
	    if(transform.localScale.x != sizer) {
            transform.localScale = Vector3.one * sizer;
        }
	}


}
