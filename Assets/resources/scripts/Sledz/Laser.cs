﻿using UnityEngine;
using System.Collections;

public class Laser : MonoBehaviour {
    private float speed = 20f;
    public GameObject explosion;
    public AudioSource explosionSoud;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        transform.position += Vector3.right * Time.deltaTime * speed;
        if(transform.position.x >= Statics.XBOUNDARIES + 4f) {
            Destroy(gameObject);
        }
    }

    public void OnTriggerEnter2D(Collider2D collision) {
        transform.rotation =  Quaternion.Euler(0, 0, -90f);
        if (collision.gameObject.tag.Equals("Enemy") || collision.gameObject.tag.Equals("Toxic")) {
            explosionSoud.Play();
            Destroy(collision.gameObject);
            Destroy(gameObject.GetComponent<Rigidbody2D>());
            StartCoroutine(explode(Instantiate(explosion, transform.position, transform.rotation)));
            SpriteRenderer renderer = gameObject.GetComponent<SpriteRenderer>();
            renderer.color = new Color(renderer.color.r,
                renderer.color.g,
                renderer.color.b,
                1f);
			ClickerManager.instance.AddPoints(SledzUpgrades.pointMultipler);
        }

    }


    private IEnumerator explode(Object explosion) {
        ((GameObject)explosion).transform.position = gameObject.transform.position + new Vector3(0.5f, 0, 0);
        yield return new WaitForSeconds(1f);
        Destroy(explosion);
        Destroy(gameObject);
    }
}
