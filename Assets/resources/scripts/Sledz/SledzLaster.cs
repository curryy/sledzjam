﻿using UnityEngine;
using System.Collections;

public class SledzLaster : MonoBehaviour {
    public TimerUtil laserTimer;
    public AudioSource laserSound;
    public GameObject laserRay;
    // Use this for initialization
    void Start() {
        laserTimer = SledzUpgrades.laserTimerUtil;
    }

    // Update is called once per frame
    void Update() {
        if (SledzUpgrades.isLaserActive) {
            laserTimer.Update();
            if (laserTimer.IsLimitReached()) {
                laserSound.Play();
                Instantiate(laserRay, transform.position, transform.rotation);
            }

        }
    }

}