﻿using System.Collections;
using UnityEngine;
using UnityStandardAssets.ImageEffects;

public class SledzMoving : MonoBehaviour {
    public EnemySpawner spawner;
    public GameObject explosion;
    public AudioSource explosionSound;
    public Camera mainCamera;


    private string W = "w";
    private string S = "s";
    private string A = "a";
    private string D = "d";
    // Use this for initialization
    void Start() {
        spawner = GameObject.Find("EnemySpawner").GetComponent<EnemySpawner>();
        mainCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
        Physics.gravity = new Vector3(0, 0, 0);
        spawner.isSpawning = true;
    }

    // Update is called once per frame
    void Update() {
        HandleMoves();
        if (Input.GetKey("r")) {
            Destroy(gameObject);
        }
    }

    private void HandleMoves() {
        if (Input.GetKey(W)) {
            if (transform.position.y <= Statics.YBOUNDARIES) {
                Move(Vector3.up);
            }

        }
        if (Input.GetKey(S)) {
            if (transform.position.y >= -Statics.YBOUNDARIES) {
                Move(Vector3.down);

            }
        }
        if (Input.GetKey(D) && SledzUpgrades.forwardMove) {
            if (transform.position.x <= Statics.XBOUNDARIES) {
                Move(Vector3.right);
            }

        }
        if (Input.GetKey(A) && SledzUpgrades.backwardMove) {
            if (transform.position.x >= -Statics.XBOUNDARIES) {
                Move(Vector3.left);
            }


        }
    }

    public void OnTriggerEnter2D(Collider2D collision) {
        EnemyCollision(collision);
        ToxicCollision(collision);
    }

    private void ToxicCollision(Collider2D collision) {
        if (collision.gameObject.tag == "Toxic") {
            if (IsBigger(collision)) {
                Destroy(gameObject);
                spawner.isSpawning = false;
            }
            else {
                mainCamera.GetComponent<BlurOptimized>().enabled = true;
                mainCamera.GetComponent<NoiseAndScratches>().enabled = true;
                W = "s";
                D = "a";
                S = "w";
                A = "d";
                Destroy(collision.gameObject);
                explosionSound.Play();
                StartCoroutine(explode(Instantiate(explosion, transform.position, transform.rotation)));
                SledzUpgrades.areWeOnDrugs = true;
            }

        }
    }

    private void EnemyCollision(Collider2D collision) {
        if (collision.gameObject.tag == "Enemy") {
            if (IsBigger(collision)) {
                Destroy(gameObject);
                spawner.isSpawning = false;

            }
            else {
                Destroy(collision.gameObject);
                if (SledzUpgrades.areExplosionsAviable) {
                    explosionSound.Play();
                    StartCoroutine(explode(Instantiate(explosion, transform.position, transform.rotation)));
                }
                ClickerManager.instance.AddPoints(30 * SledzUpgrades.pointMultipler);
            }
        }
    }

    private bool IsBigger(Collider2D collision) {
        return collision.gameObject.transform.localScale.x > transform.localScale.x;
    }

    private IEnumerator explode(Object explosion) {
        ((GameObject)explosion).transform.position = gameObject.transform.position + new Vector3(0.5f,0,0);
        yield return new WaitForSeconds(1f);
        Destroy(explosion);
    }

    private void Move(Vector3 direction) {
        gameObject.transform.position += direction * SledzUpgrades.speed * Time.deltaTime;
    }

    public void TurnOffDrugMode() {
        mainCamera.GetComponent<BlurOptimized>().enabled = false;
        mainCamera.GetComponent<NoiseAndScratches>().enabled = false;
        W = "w";
        D = "d";
        S = "s";
        A = "a";
    }

    
}
