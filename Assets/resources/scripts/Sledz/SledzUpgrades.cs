﻿using UnityEngine;
using System.Collections;

public class SledzUpgrades : MonoBehaviour {
    //move
    public static float speed = 1f;
    public static bool forwardMove = false;
    public static bool backwardMove = false;
    //enemys spawn
    public static float spawnLimit = 1f;
    public static int spawnAtTime = 1;

    public static TimerUtil laserTimerUtil = new TimerUtil(5f);


    public static void ChangeSpawnTimeLimit(float newTime) {
        EnemySpawner spawner = GameObject.Find("EnemySpawner").GetComponent<EnemySpawner>();
        spawner.spawnTime.ChangeLimit(newTime);
    }


    //Laser
    public static bool isLaserActive = false;
    public static float laserTimer = 5f;

    public static void ChangeLaserTimeLimit(float newTime) {

        laserTimerUtil.ChangeLimit(newTime);
    }

    //Explosions
    public static bool areExplosionsAviable = false;


    //Drugs
    public static bool areWeOnDrugs = false;

    public static int pointMultipler = 10;

}