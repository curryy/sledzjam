﻿using UnityEngine;
using System.Collections;

public class ShopScript : MonoBehaviour {

    UpgradesList upgradeList;
    UpgradeModel tmpItem;
    // Use this for initialization
    void Start () {
        upgradeList = new UpgradesList();
        tmpItem = null;
        ButtonScript b1 = GameObject.Find("b1").GetComponent<ButtonScript>();
        ButtonScript b2 = GameObject.Find("b2").GetComponent<ButtonScript>();
        ButtonScript b3 = GameObject.Find("b3").GetComponent<ButtonScript>();
        ButtonScript b4 = GameObject.Find("b4").GetComponent<ButtonScript>();
        ButtonScript b5 = GameObject.Find("b5").GetComponent<ButtonScript>();
        ButtonScript b6 = GameObject.Find("b6").GetComponent<ButtonScript>();
        ButtonScript b7 = GameObject.Find("b7").GetComponent<ButtonScript>();
        ButtonScript b8 = GameObject.Find("b8").GetComponent<ButtonScript>();
        ButtonScript b9 = GameObject.Find("b9").GetComponent<ButtonScript>();
        ButtonScript b10 = GameObject.Find("b10").GetComponent<ButtonScript>();
        ButtonScript b11 = GameObject.Find("b11").GetComponent<ButtonScript>();
        ButtonScript b12 = GameObject.Find("b12").GetComponent<ButtonScript>();
        ButtonScript b13 = GameObject.Find("b13").GetComponent<ButtonScript>();
        ButtonScript b14 = GameObject.Find("b14").GetComponent<ButtonScript>();

        b1.setText(upgradeList.getItemAt(0).name + "  - " + upgradeList.getItemAt(0).price);
        b2.setText(upgradeList.getItemAt(1).name + "  - " + upgradeList.getItemAt(1).price);
        b3.setText(upgradeList.getItemAt(2).name + "  - " + upgradeList.getItemAt(2).price);
        b4.setText(upgradeList.getItemAt(3).name + "  - " + upgradeList.getItemAt(3).price);
        b5.setText(upgradeList.getItemAt(4).name + "  - " + upgradeList.getItemAt(4).price);
        b6.setText(upgradeList.getItemAt(5).name + "  - " + upgradeList.getItemAt(5).price);
        b7.setText(upgradeList.getItemAt(6).name + "  - " + upgradeList.getItemAt(6).price);
        b8.setText(upgradeList.getItemAt(7).name + "  - " + upgradeList.getItemAt(7).price);
        b9.setText(upgradeList.getItemAt(8).name + "  - " + upgradeList.getItemAt(8).price);
        b10.setText(upgradeList.getItemAt(9).name + "  - " + upgradeList.getItemAt(9).price);
        b11.setText(upgradeList.getItemAt(10).name + "  - " + upgradeList.getItemAt(10).price);
        b12.setText(upgradeList.getItemAt(11).name + "  - " + upgradeList.getItemAt(11).price);
        b13.setText(upgradeList.getItemAt(12).name + "  - " + upgradeList.getItemAt(12).price);
        b14.setText(upgradeList.getItemAt(13).name + "  - " + upgradeList.getItemAt(13).price);
       

        b1.setUpgrade(upgradeList.getItemAt(0));
        b2.setUpgrade(upgradeList.getItemAt(1));
        b3.setUpgrade(upgradeList.getItemAt(2));
        b4.setUpgrade(upgradeList.getItemAt(3));
        b5.setUpgrade(upgradeList.getItemAt(4));
        b6.setUpgrade(upgradeList.getItemAt(5));
        b7.setUpgrade(upgradeList.getItemAt(6));
        b8.setUpgrade(upgradeList.getItemAt(7));
        b9.setUpgrade(upgradeList.getItemAt(8));
        b10.setUpgrade(upgradeList.getItemAt(9));
        b11.setUpgrade(upgradeList.getItemAt(10));
        b12.setUpgrade(upgradeList.getItemAt(11));
        b13.setUpgrade(upgradeList.getItemAt(12));
        b14.setUpgrade(upgradeList.getItemAt(13));

    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
